﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "test_dbDataSet.message". При необходимости она может быть перемещена или удалена.
            this.messageTableAdapter.Fill(this.test_dbDataSet.message);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "test_dbDataSet.entry_record". При необходимости она может быть перемещена или удалена.
            this.entry_recordTableAdapter.Fill(this.test_dbDataSet.entry_record);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "test_dbDataSet.users". При необходимости она может быть перемещена или удалена.
            this.usersTableAdapter.Fill(this.test_dbDataSet.users);

        }
    }
}
